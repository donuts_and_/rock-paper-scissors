//
//  RockPaperScissorsApp.swift
//  RockPaperScissors
//
//  Created by Tran, Bryant on 8/26/21.
//

import SwiftUI

@main
struct RockPaperScissorsApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
