//
//  ContentView.swift
//  RockPaperScissors
//
//  Created by Tran, Bryant on 8/26/21.
//

import SwiftUI

struct Move: Identifiable {
    let name: String
    let winning: String
    let losing: String
    var id: String { name }
    
    static var Rock = Move(name: "Rock", winning: "Paper", losing: "Scissors")
    static var Paper = Move(name: "Paper", winning: "Scissors", losing: "Rock")
    static var Scissors = Move(name: "Scissors", winning: "Rock", losing: "Paper")
}

struct BigText: ViewModifier {
    func body(content: Content) -> some View {
        content
            .foregroundColor(.blue)
            .font(.largeTitle)
    }
}

extension View {
    func bigText() -> some View {
        return self.modifier(BigText())
    }
}

struct ContentView: View {
    static let moves = [Move.Rock, Move.Paper, Move.Scissors]
    
    @State private var move = moves.randomElement()!
    @State private var shouldWin = Bool.random()
    @State private var score = 0
    @State private var status = "Make your move..."
    
    var body: some View {
        VStack(spacing: 30) {
            VStack {
                Text("Opponent's Move:")
                Text(move.name)
                    .bigText()
            }
            
            VStack(spacing: 10) {
                Text("Select a \(shouldWin ? "winning" : "losing") move!")
                ForEach(ContentView.moves) { moveType in
                    Button(moveType.name) {
                        challenge(moveType)
                    }
                    .foregroundColor(.white)
                    .frame(width: 120, height: 36, alignment: .center)
                    .background(Color.blue)
                    .clipShape(Capsule())
                }
            }
            
            VStack {
                Text(status)
                Text("Score: \(score)")
            }
        }
    }
    
    func challenge(_ choice: Move) {
        let desired = shouldWin ? move.winning : move.losing
        if choice.name == desired {
            status = "Correct!"
            score += 1
        } else {
            status = "Wrong!"
            score -= 1
        }

        // Reset
        move = ContentView.moves.randomElement()!
        shouldWin = Bool.random()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
